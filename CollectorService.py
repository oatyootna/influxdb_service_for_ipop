import time
import logging
import datetime
import threading
from collections import defaultdict

from flask import abort, request
from InfluxCollector import InfluxCollector
from MongoCollector import MongoCollector

class CollectorServiceInstance(object):
    def __init__(self, config_dict):

        self._db =  InfluxCollector(config_dict) 
       
        self._data_held_lock = threading.Lock()

        self.intr_or_term = False

        with self._data_held_lock:
            self._reset_data_held()

        self.flush_duration = self._db.flush_duration

        self._logger = \
            logging.getLogger("network_visualizer.collector_service")

    def _reset_data_held(self):
        self._db._reset_data_held()

    def process_update_req(self, node_id):
        """
        Serves the PUT request from the OverlayVisualizer controller module
        """

        self._logger.info("Received request {} for node_id"
                          " {}".format(request.json, node_id))

        if self.intr_or_term:
            self._logger.warn("Rejecting request with a 500 because of"
                              " SIGINT/SIGTERM!")
            return abort(500)

        try:
            # NOTE request is instantiated when this method is registered
            # as a view_func in a flask container
            req = request.json
            req_data = req["VizData"]
            
            self._data_held_lock.acquire()
                
            self._db.build_data(req, req_data, node_id)
                #self._logger.debug(
                 #   "Updating node data for node_id {}".format(node_id))


        except KeyError as e:
            self._logger.exception(e)
        #print(req_data[ovrl_id][mod_name]) 
        # Process data for arbitrary modules now that we are done
        # processing data for LinkManager
       # for mod_name in req_data[ovrl_id]:
        #    if mod_name != "LinkManager":
         #       if mod_name not in self.data_held:
          #          self.data_held[mod_name] = defaultdict(dict)
            #    self.data_held[mod_name][ovrl_id][node_id] = \
           #         req_data[ovrl_id][mod_name]
        
        self._data_held_lock.release()

        return "Success"

    def dump_vis_data(self):
        """Runs in a separate thread (see constructor)"""

        while not self.intr_or_term:
            self._logger.debug(
                    "Sleeing for {} seconds".format(
                            self.flush_duration))
            time.sleep(self.flush_duration)

            self._data_held_lock.acquire()
            if self._db.data_held["Overlays"]:
                #self.data_held["_id"] = \
                 #       datetime.datetime.utcnow().replace(microsecond=0)
                self._logger.debug(
                    "Beginning mongo dump on document {}"
                    .format(self._db.data_held))
                
                self._db.insert_data() 
                #self._logger.debug("Mongo dump successful")

                self._reset_data_held()

            self._data_held_lock.release()

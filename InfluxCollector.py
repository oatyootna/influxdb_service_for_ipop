from DatabasePool import DatabasePoolInstance
from influxdb import InfluxDBClient
from collections import defaultdict
import datetime
import re
import json

class InfluxCollector(DatabasePoolInstance):
    def __init__(self, config_dict):
        database_name = config_dict["influx"]["dbname"]
        
        self._db = \
            InfluxDBClient(
                config_dict["influx"]["host"],
                config_dict["influx"]["port"],
                database= database_name
            )
        self.flush_duration = config_dict['flush_duration']
        # check if database is exist
        if not{'name': database_name} \
                            in self._db.get_list_database():
             self._db.create_database(database_name)
    

    def _reset_data_held(self):
        self.data_held = {
            "Overlays": list(),
            "Nodes": list(),
            "Links": list(),
            "ChannelProperties" : list(),
            "IceProperties" :list(),
            "LogEntry": list(),
        }
        self._link_ids = defaultdict(set)

    def build_data(self, req, req_data, node_id ):   
           
            overlay_list = list()
            node_list = list()
            
            #print(json.dumps(req, indent=2 ))
            
            for ovrl_id in req_data:
                #self._logger.debug("Processing data for overlay_id"
                 #                  " {}".format(ovrl_id))
                if ovrl_id not in list(overlay['overlay_id'] for overlay in self.data_held["Overlays"]):
                    ovrl_init_data = {
                        "overlay_id": ovrl_id,
                        "num_nodes": 0,
                        "num_links": 0,
                        "name": "",
                        "description" : ""
                    }
                    if "Name" in  req_data[ovrl_id]["Topology"] and "Description" in req_data[ovrl_id]["Topology"]:
                        
                        ovrl_init_data.update({"name":req_data[ovrl_id]["Topology"]["Name"],
                            "description": req_data[ovrl_id]["Topology"]["Description"]})

                    self.data_held["Overlays"].append(ovrl_init_data)
       
                     
                # Increment node counter in overlay if we did not have its data
                # for ovrl_id (meaning it is new in this overlay)
                # NOTE! This must be done before self.data_held["Nodes"] is
                # updated with node_data as it will add the key ovrl_id causing
                # this test to not behave as desired
                
                #find the overlay dict
                overlay_dicts = next((ovl_dict for ovl_dict in self.data_held['Overlays'] \
                                        if ovl_dict['overlay_id'] == ovrl_id))

                #find the list of node 
                node_dicts = list(node_dict for node_dict in self.data_held['Nodes'] \
                                        if node_dict['overlay_id'] == ovrl_id)
                
                if node_id not in list(id['node_id'] for id in node_dicts):
                     overlay_dicts.update({"num_nodes": int(overlay_dicts['num_nodes']) + 1})
                
                # TODO handle removal of a node within an interval

                #self._logger.debug(
                 #   "Updating node data for node_id {}".format(node_id))

                # Add the optional human-readable node name (if provided)
                node_data = dict()
                node_data['overlay_id'] = ovrl_id
                node_data['node_id'] = node_id
                node_data["geo_coordinate"] = "Geo Coordinate Not Found"
                node_data["node_name"] = None

                if "GeoCoordinate" in req:
                    node_data["geo_coordinate"] = req["GeoCoordinate"]

                if "NodeName" in req:
                    node_data["node_name"] = req["NodeName"]
                
                
                self.data_held["Nodes"].append(node_data)
             
                # Add/update data link data for the reporting node
                if "LinkManager" in req_data[ovrl_id] \
                        and req_data[ovrl_id]["LinkManager"]:
                    link_manager_data = req_data[ovrl_id]["LinkManager"]
                    
                    for link_id in link_manager_data[node_id]:
                        req_link_data = \
                            link_manager_data[node_id][link_id]
                        
                        if "ConnectionEdges" in req_data[ovrl_id]["Topology"]:
                            try:
                                link_topo = req_data[ovrl_id]["Topology"]['ConnectionEdges'][link_id]
                            except:
                                break
                        else:
                            try:
                                link_topo = req_data[ovrl_id]["Topology"][link_id]
                            except:
                                break
                            
                        try:
                            tap = req_link_data["TapName"]
                        except:
                            tap = ""

                        link_data = {
                             "overlay_id": ovrl_id ,
                             "link_id": link_id,
                             "node_id": req_link_data['NodeId'],
                             "peer_id": link_topo["PeerId"],
                             "created_time": link_topo["CreatedTime"],
                             "connected_state": link_topo["State"] ,
                             "connected_time": link_topo['ConnectedTime'],
                             "edge_type": link_topo["Type"],
                             "tap_name": tap,
                             "mac": req_link_data["MAC"],
                             "overlay_IP4": "" 
                        }
                        
                        # Increment link counter in overlay if we did not have
                        # its data for ovrl_id (meaning it is new in this
                        # overlay)
                        
                        if "NumEdges" in req_data[ovrl_id]['Topology']:
                            numEdges = int(req_data[ovrl_id]['Topology']['NumEdges'])
                            if numEdges != int(overlay_dicts['num_links']):
                                overlay_dicts.update({"num_links": numEdges })
                        else:
                            if link_id not in self._link_ids[ovrl_id]:
                                self._link_ids[ovrl_id].add(link_id)
                                overlay_dicts.update({"num_links": int(overlay_dicts['num_links']) + 1 })
                         

                        # Due to Stats field in req_link_data have a posibility to disappear.
                        # Need to check is there stats or not. 
                        try:
                            # Because of there are many Stats.
                            # To handdle that issue, need to check best_con == True.
                            link_stat = next(( stat_dict for stat_dict in req_link_data["Stats"] \
                                    if stat_dict["best_conn"] is True))

                            best_conn = link_stat["best_conn"]
                            total_byte_sent = link_stat["sent_total_bytes"]
                            byte_sent = link_stat["sent_bytes_second"]
                            byte_receive =  link_stat["recv_bytes_second"]
                            total_byte_receive = link_stat["recv_total_bytes"]
                            latency = link_stat["rtt"]
                            local_addr = link_stat["local_candidate"].split(":")[5]
                            remote_addr = link_stat["remote_candidate"].split(":")[5]

                        except KeyError:
                            best_conn = None
                            total_byte_sent = 0
                            byte_sent = 0
                            byte_receive =  0
                            total_byte_receive = 0
                            latency = 0
                            local_addr = ""
                            remote_addr = ""


                        channel_data = {
                                "link_id" : link_id,
                                "node_id" : node_id,
                                "byte_sent": byte_sent,
                                "total_byte_sent" : total_byte_sent,
                                "byte_receive" : byte_receive,
                                "total_byte_receive": total_byte_receive
                                }
                        ice_data = {
                                "link_id" : link_id,
                                "node_id" : node_id,
                                "role" :"",
                                "best_connection":  best_conn,
                                "local_addr": local_addr,
                                "remote_addr": remote_addr,
                                "latency": latency,
                                }
                        if "IceRole" in req_link_data:
                            ice_data.update({"role": req_link_data["IceRole"]})
                

                        self.data_held["Links"].append(link_data)
                        self.data_held["ChannelProperties"].append(channel_data)
                        self.data_held["IceProperties"].append(ice_data)
                        #self.data_held["LogEntry"].append(log_entry_data)
                        
                #for mod_name in req_data[ovrl_id]:
                 #      if mod_name != "LinkManager":
                   #        if mod_name not in self.data_held:
                    #           print(req_data[ovrl_id][mod_name])
               
    def insert_data(self):
            time_stamp =  datetime.datetime.utcnow().replace(microsecond=0)
            points = list()
            
            
            self.add_points_dict('Overlays',['overlay_id'],points , time_stamp) 
            self.add_points_dict('Nodes',['node_id', 'overlay_id'], points, time_stamp)
            self.add_points_dict('Links',['node_id', 'link_id', 'overlay_id'],points, time_stamp)
            self.add_points_dict('ChannelProperties',['node_id','link_id'],points, time_stamp)
            self.add_points_dict('IceProperties',['node_id','link_id'],points, time_stamp)
            #self.add_points_dict('LogEntry','overlay_id',points, time_stamp)


            #insert data into the Influx
            self._db.write_points(points)

    # For restructure data format 
    def add_points_dict(self, key_name, tags, points, time_stamp):

        for data in self.data_held[key_name] :
                tagDict = dict()
                for tag in tags:
                  tagDict[tag] = data[tag]
                  del data[tag]

                points.append( {
                    "measurement":key_name,
                    "tags": tagDict,
                    "time": time_stamp,
                    "fields": data
                })

                
    # receive methods GET
    def get_time(self,time_db):
        sp_time = re.split("T|\.|Z",time_db)
        time = sp_time[0] + " " + sp_time[1]
        return time

    def get_set_id(self, list_data, id):
        set_data = set()
        for data in list_data:
            set_data.add(data[id])
        return set_data

    def get_influx_time(self,timestamp):
        len_time = len(str(timestamp))
        influx_time = 0
        if len_time < 19: 
            influx_time = timestamp*10**(19-len_time)
        elif len_time == 19:
            influx_time = timestamp
        return influx_time
    def log_error_data(self, measurement_name, query, timestamp):
        print("------------------------------------------------------------------------")
        print("No data in "+measurement_name)
        print("Query from InfluxDB = "+query)
        print("time = ", datetime.datetime.fromtimestamp(timestamp))
        print("------------------------------------------------------------------------")

    # /IPOP/overlays
    def get_overlays(self,timestamp):
        # timestamp_influx = timestamp*10**9
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Overlays where time <= " + \
            str(timestamp_influx)+" and time > " + \
            str(flush_timestamp)
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Overlays'))
        set_db = self.get_set_id(listdb,"overlay_id")
        frame = '{"current_state":{}}'
        json_data = json.loads(frame)
        for overlay_id in set_db:
            temp = self.get_single_overlay(overlay_id, timestamp)
            # temp = self.get_single_overlay("102000F", timestamp)
            json_data["current_state"].update(temp)
        try:
            time = {"intervalNo": self.get_time(listdb[0]["time"])}
            json_data.update(time)
        except IndexError:
            self.log_error_data("Overlays", query, timestamp)
        return json_data
    
    def get_single_overlay(self,overlay_id,timestamp):
        # timestamp_influx = timestamp*10**9
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Overlays where time <= " + \
            str(timestamp_influx)+" and time > " + \
            str(flush_timestamp)+" and overlay_id = \'"+overlay_id+"\' ORDER BY DESC LIMIT 1"
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Overlays'))
        # print("------------------------------------------------------------------------")
        # print(listdb)
        # print("------------------------------------------------------------------------")
        try:
            json_data = {
                listdb[0]["overlay_id"]:
                {
                    "num_nodes": listdb[0]["num_nodes"],
                    "num_links": listdb[0]["num_links"],
                    "name": listdb[0]["name"],
                    "description": listdb[0]["description"]
                }
            }
        except IndexError:
            json_data = {}
            self.log_error_data("Overlay", query, timestamp)
        
        return json_data
        
    
        
    # /IPOP/overlays/<overlayid>/nodes/<nodeid>
    def get_single_node(self, overlay_id, node_id, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        query = "select * from Nodes where \"overlay_id\" = \'" + overlay_id + \
            "\' AND node_id = \'"+node_id+"\' AND time <= " + \
                str(timestamp_influx)+" and time > " + \
                str(flush_timestamp)+" ORDER BY DESC LIMIT 1"
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Nodes'))
        # print(db)
        try:
            json_data = {
                overlay_id:
                {
                    node_id:
                    {
                        "node_name": listdb[0]["node_name"],
                        "geo_coordinate": listdb[0]["geo_coordinate"]
                    }
                },
                "intervalNo": self.get_time(listdb[0]["time"])
            }
        except IndexError:
            json_data = {}
            self.log_error_data("Node", query, timestamp)

        return json_data
    
    # /IPOP/overlays/<overlayid>/nodes
    def get_nodes(self, overlay_id, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        query = "select * from Nodes where time <= " + \
                str(timestamp_influx)+" and time > " + \
            str(flush_timestamp)+" and \"overlay_id\" = \'" \
            + overlay_id +"\'"
        # print("------------------------------------------------------------------------")
        # print(query)
        # print("------------------------------------------------------------------------")
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Nodes'))
        set_db = self.get_set_id(listdb, "node_id")
        frame = '{\"'+overlay_id+'\":{"current_state":{}}}'
        json_data = json.loads(frame)
        for node_id in set_db:
            temp = self.get_single_node(overlay_id, node_id, timestamp)
            json_data[overlay_id]["current_state"].update(temp[overlay_id])
        try:
            time = {"intervalNo": self.get_time(listdb[0]["time"])}
            json_data.update(time)
        except IndexError:
            self.log_error_data("Nodes", query, timestamp)
        
        return json_data


    def get_iceProperties(self,link_id,node_id,timestamp):
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        query = "select * from IceProperties where time <= " + str(timestamp_influx)+" and time > " + \
            str(flush_timestamp)+" and \"link_id\"  = \'"+link_id + \
            "\' and \"node_id\" = \'"+node_id+"\' ORDER BY DESC LIMIT 1"
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='IceProperties'))
        try:
            json_data = {
                link_id:
                {
                    node_id:
                    {
                        "role": listdb[0]["role"],
                        "best_connection": listdb[0]["best_connection"],
                        "local_addr": listdb[0]["local_addr"],
                        "remote_addr": listdb[0]["remote_addr"],
                        "latency": listdb[0]["latency"]
                    }
                }
            }
        except IndexError:
            json_data = {}
            self.log_error_data("iceProperties", query, timestamp)
        return json_data

    def get_channelProperties(self,link_id, node_id, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        query = "select * from ChannelProperties where time <= " + \
            str(timestamp_influx)+" and time > " + \
            str(flush_timestamp)+" and \"link_id\"  = \'"+link_id + \
            "\' and \"node_id\" = \'"+node_id+"\' ORDER BY DESC LIMIT 1"
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='ChannelProperties'))
        iceProperties = self.get_iceProperties(link_id, node_id, timestamp)
        try:
            json_data = {
                link_id:
                {
                    node_id:
                    {
                        "byte_sent": listdb[0]["byte_sent"],
                        "total_byte_sent": listdb[0]["total_byte_sent"],
                        "byte_receive": listdb[0]["byte_receive"],
                        "total_byte_receive": listdb[0]["total_byte_receive"],
                        "IceProperties": iceProperties[listdb[0]["link_id"]][listdb[0]["node_id"]]
                    }
                }
            }
        except IndexError:
            json_data = {}
            self.log_error_data("channelProperties", query, timestamp)
        return json_data

    def get_single_link(self, overlay_id, link_id, node_id, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        query = "select * from Links where time <= " + \
                str(timestamp_influx)+" and time > " + \
            str(flush_timestamp)+" and \"overlay_id\"  = \'"+overlay_id + \
            "\' and link_id = \'"+link_id+"\' and node_id = \'" + \
            node_id+"\' ORDER BY DESC LIMIT 1"
        db = self._db.query(query)
        # print("------------------------------------------------------------------------")
        # print(query)
        # print("------------------------------------------------------------------------")
        listdb = list(db.get_points(measurement='Links'))
        channelProperties = self.get_channelProperties(link_id, node_id, timestamp)
        try:
            json_data = {
                listdb[0]["node_id"]:
                {
                    listdb[0]["link_id"]:
                    {
                        "tap_name": listdb[0]["tap_name"],
                        "mac": listdb[0]["mac"],
                        "SrcNodeID": listdb[0]["node_id"],
                        "TgtNodeID": listdb[0]["peer_id"],
                        "peer_id": listdb[0]["peer_id"],
                        "state": listdb[0]["connected_state"],
                        "EdgeID": listdb[0]["link_id"],
                        "tap_name": listdb[0]["tap_name"],
                        "connected_time": listdb[0]["connected_time"],
                        "created_time": listdb[0]["created_time"],
                        "edge_type": listdb[0]["edge_type"],
                        "ChannelProperties": channelProperties[listdb[0]["link_id"]][listdb[0]["node_id"]]
                    }
                }
            }
        except IndexError:
            json_data = {}
            self.log_error_data("Link", query, timestamp)
        
        return json_data
    # /IPOP/overlays/<overlayid>/nodes/<nodeid>/links
    def get_links_for_a_node(self, overlay_id, node_id, timestamp):
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        query = "select * from Links where time <= " + \
                str(timestamp_influx)+" and time > " + \
            str(flush_timestamp)+" and overlay_id  = \'"+overlay_id + \
            "\' and node_id = \'"+node_id+"\'"
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Links'))
        set_db = self.get_set_id(listdb, "link_id")
        frame = '{\"'+overlay_id+'\":{\"'+node_id+'\":{"current_state":{}}}}'
        json_data = json.loads(frame)

        for link_id in set_db:
            temp = self.get_single_link(overlay_id, link_id, node_id, timestamp)
            json_data[overlay_id][node_id]["current_state"].update(temp[node_id])
        try:
            time = {"intervalNo": self.get_time(listdb[0]["time"])}
            json_data.update(time)
        except IndexError:
            self.log_error_data("Links_for_a_node", query, timestamp)
        return json_data
        
    # /IPOP/overlays/<overlayid>/links
    def get_links_in_an_overlay(self, overlay_id, timestamp):
        timestamp_influx = self.get_influx_time(timestamp)
        flush_timestamp = timestamp_influx-(self.flush_duration*10**9)
        # dbClient = InfluxDBClient('localhost', 8086, 'root', 'root', 'ipopdb')
        query = "select * from Links where time <= " + \
                str(timestamp_influx)+" and time > " + \
            str(flush_timestamp)+" and overlay_id  = \'"+overlay_id+"\'"
        # print(query)
        db = self._db.query(query)
        listdb = list(db.get_points(measurement='Links'))
        frame = '{\"'+overlay_id+'\":{"current_state":{}}}'
        set_db = self.get_set_id(listdb, "node_id")
        json_data = json.loads(frame)

        for node_id in set_db:
            temp = self.get_links_for_a_node(overlay_id, node_id, timestamp)
            # print(temp)
            frame_node = {node_id: {}}
            json_data[overlay_id]["current_state"].update(frame_node)
            json_data[overlay_id]["current_state"][node_id].update(temp[overlay_id][node_id]["current_state"])
            
        try:
            time = {"intervalNo": self.get_time(listdb[0]["time"])}
            json_data.update(time)
        except IndexError:
            self.log_error_data("Links_in_an_overlay", query, timestamp)
        return json_data


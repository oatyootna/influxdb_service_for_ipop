#!/usr/bin/env python

import time, json, sys, logging
from datetime import timezone
import datetime
# from datetime import datetime
from flask import Flask, make_response, render_template, request
from InfluxCollector import InfluxCollector
from MongoCollector import MongoCollector

class CentralVisualizerService(object):
    # def __init__(self,config_dict):
    #     _mc = MongoClient()
    #     _ipopdb = _mc[config_dict["mongo"]["dbname"]]
    #     self._mongo_data = _ipopdb[config_dict["mongo"]["collection_name"]]
    #     self._logger = logging.getLogger("network_visualizer.central_visualizer")

    def __init__(self, config_dict):
        self._db = InfluxCollector(config_dict)

    # def _get_current_state(self,current_doc,requested_interval):
    #     response_msg = {
    #                     "current_state": current_doc,
    #                     "intervalNo":str(requested_interval)
    #                     }
    #     return response_msg

    # def _find_diff_between_intervals(self,new_doc,old_doc,requested_interval,
    #                                  is_links=False):
    #     added = { key : new_doc[key] for key in set(new_doc) - set(old_doc) }
    #     removed = { key : old_doc[key] for key in set(old_doc) - set(new_doc) }
    #     existing = list(set(new_doc) - set(added))
    #     modified ={}
    #     for key in existing:
    #         if is_links or frozenset(new_doc[key].items()) != frozenset(old_doc[key].items()) :
    #             modified[key] = new_doc[key]

    #     response_msg = {
    #                     "removed": removed,
    #                     "added": added,
    #                     "modified": modified,
    #                     "intervalNo": str(requested_interval),
    #                     "updateOffset": 15          # To be modified
    #                     }
    #     return response_msg

    # #route('/IPOP')
    # def homepage(self):
    #     resp =  render_template('ipop_mainpage.html')
    #     return resp

    # #route('/IPOP/intervals', methods=['GET'])
    # def get_intervals(self):
    #     start_time = datetime.strptime(request.args.get('start') , "%Y-%m-%dT%H:%M:%S")
    #     end_time = datetime.strptime(request.args.get('end') , "%Y-%m-%dT%H:%M:%S")
    #     interval_numbers = []
    #     for docs in self._mongo_data.find({"_id": {"$gt": start_time , "$lt": end_time}}, {"_id":1}):
    #         interval_numbers.append(str(docs["_id"]))
    #     response_msg = {"IPOP" : {
    #                             "IntervalNumbers" : interval_numbers
    #                     }}
    #     resp = make_response(json.dumps(response_msg))
    #     resp.headers['Content-Type'] = "application/json"
    #     return resp

    
    def get_timestamp_utc(self,req_interval):
        interval = datetime.datetime.strptime(req_interval, "%Y-%m-%dT%H:%M:%S")
        utc_time = interval.replace(tzinfo = timezone.utc)
        timestamp = utc_time.timestamp()
        return int(timestamp)

    #route('/IPOP/overlays', methods=['GET'])
    def get_overlays(self):
        timestamp = self.get_timestamp_utc(request.args.get('interval'))
        resp = self._db.get_overlays(timestamp)
        return resp

    #route('/IPOP/overlays/<overlayid>/nodes', methods=['GET'])
    def get_nodes_in_an_overlay(self,overlayid):
        timestamp = self.get_timestamp_utc(request.args.get('interval'))
        resp = self._db.get_nodes(overlayid, timestamp)
        return resp

    #route('/IPOP/overlays/<overlayid>/nodes/<nodeid>', methods=['GET'])
    def get_single_node(self,overlayid,nodeid):
        timestamp = self.get_timestamp_utc(request.args.get('interval'))
        resp = self._db.get_single_node(overlayid, nodeid, timestamp)
        return resp

    #route('/IPOP/overlays/<overlayid>/links', methods=['GET'])
    def get_links_in_an_overlay(self,overlayid):
        # current_state = request.args.get('current_state')
        timestamp = self.get_timestamp_utc(request.args.get('interval'))
        resp = self._db.get_links_in_an_overlay(overlayid, timestamp)
        return resp

    #route('/IPOP/overlays/<overlayid>/nodes/<nodeid>/links', methods=['GET'])
    def get_links_for_a_node(self,overlayid,nodeid):
        timestamp = self.get_timestamp_utc(request.args.get('interval'))
        resp = self._db.get_links_for_a_node(overlayid, nodeid, timestamp)
        return resp
